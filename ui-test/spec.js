const common_utils = require('./commonUtils');
const timestamp = require('time-stamp');
var cc_project = require('./cc.po');

var textVerForCLPage = "and here is our customer list";
var data_val_text = "Please provide your name";
var EC = protractor.ExpectedConditions;
var currentDate;
var userName;

describe('CMS POC', function () {
    browser.waitForAngularEnabled(false);
    userName = common_utils.generateUserName();
    currentDate = timestamp('DD YYYY');

    jasmine.DEFAULT_TIMEOUT_INTERVAL = 50000;

    // beforeEach(() => {
    //     browser.waitForAngularEnabled(false);
    //     //browser.manage().timeouts().implicitlyWait(5000);
    // })

    // beforeAll(() => {
        // browser.manage().timeouts().implicitlyWait(15000);
        // browser.waitForAngularEnabled(false);
        // userName = common_utils.generateUserName();
        // currentDate = timestamp('DD YYYY');
    // })
});

it('have an input text', async () => {
    await browser.get('http://localhost:3000/');
    browser.wait(EC.visibilityOf(element(by.cssContainingText('h1', 'Welcome to Customer App'))), 10000);
    //browser.sleep(40000);
    expect(cc_project.textInputField.getText()).toEqual('Please provide your name:');
});

it('data in form field should be validated',  () => {
    cc_project.inputFieldForm.click();
    cc_project.inputFieldForm.sendKeys();
    cc_project.submitBtn.click();
     browser.wait(EC.alertIsPresent(), 10000).then(function () {
         expect(EC.alertIsPresent).toBeTruthy();
        browser.switchTo().alert().accept();
     });
        //expect(cc_project.submitBtn.isDisplayed() && cc_project.inputFieldForm.isDisplayed() && EC.not(EC.elementToBeClickable(cc_project.submitBtn, cc_project.inputFieldForm)));
});

it('name and date should be verified',  () => {
    cc_project.inputFieldForm.click();
    cc_project.inputFieldForm.sendKeys(userName);
    cc_project.submitBtn.click();
     //textCLPageElement.isDisplayed().then(function() {
        expect(element(by.xpath('//div/p/b[contains(text(), ' + '"' + userName + '"' + ')]')).getText()).toEqual(userName);
        expect(element(by.xpath('//div/p/b[contains(text(), ' + '"' + currentDate + '"' + ')]')).getText()).toContain(currentDate);
    //})
}) 

//verify table of customers
//verify "No contact info available" for United Brands
//verify back button 

//dockerize with headless browser
//dockerize integration tests