
var cc_project = (function () {

    return {
        textInputField:element(by.css('#root > div > div > div > p:nth-child(1)')),
        inputFieldForm:element(by.css('input[id="name"]')),
        submitBtn:element(by.css('input[type="button"]')),
         textCLPageElement:element(by.xpath("(//*[contains(${textVerForCLPage})])")),
         data_val_text:"Please provide your name",
    }
})();

module.exports = cc_project;