const expect = require('chai').expect;
// express = require('express');
// app = express();
request = require('supertest');
request = request('http://127.0.0.1:2550');

timestamp = require('time-stamp');
let currentDate = timestamp('DD YYYY');
console.log(currentDate);


describe('API Test for POST Request', () => {
//    let name1 = "Marat Mamin";
    var name2 = Math.random().toString(36).substring(7);

    // it('should return specific values', (done) => {
    //     request
    //     .post('/')
    //     .send({"name": name1})
    //     .end((err, response) => {
    //         if (err) done (err);
    //         console.log((response.text));
    //         // let timestampString = response.text;
    //         // console.log(timestampString[1].name);
    //         // //console.log(response.text);
    //         // JSON.parse(response.text);
    //         // var inter = ((response['text']));
    //         interT = {"new": JSON.parse(response.text)};
    //         console.log(interT);
    //         expect(timestamp(interT.new.timestamp)).to.include(currentDate);
    //         //console.log(JSON.stringify(response));
    //         expect(response.statusCode).to.equal(200);
    //         var array0 = JSON.parse(response.text);
    //         for (var item in array0.customers) { var array1 = (item.id);}
    //         for (var ent in array1) {var array2 = (array1.contactInfo); expect(array2).to.exist}
    //         //expect(array1.).to.exist;
    //         //expect(response.text["customers"]["id"]["contactInfo"]).to.exist;
    //         done();
    //     })
    // })

    it('verify response content', (done) => {
        request
        .post('/')
        .send({"name": name2})
        .end((err, response) => {
            if (err) done (err);
            console.log((response.text));
            // let timestampString = response.text;
            // console.log(timestampString[1].name);
            //console.log(response.text);
            //JSON.parse(response.text);
            //var inter = response.text;
            interT = JSON.parse(response.text);
            console.log(interT);
            interTC = (interT.customers);
            console.log(interTC[0].contactInfo);
            //.contactInfo.name)
            //to ensure that the objects exist when customer name is known - should be conducted as separate method
            //for (var item of interT.customers) {
            expect(response.statusCode).to.equal(200);
            expect(timestamp(interT.timestamp)).to.include(currentDate);
            expect(interT.name).to.equal(name2);
            expect(interTC).to.exist;
            for (var i = 0; i < interTC.length; i++) {

                //skipping over missing contact info for United Brands
                (i === 3) ? i++ :  
                console.log(interTC[i].contactInfo); 

                //contact info object verification
                expect(interTC[i].contactInfo)
                .to.be.an('object')
                .that.includes.all.nested.keys('name', 'email')
                .and.not.be.undefined;

                //# of employees corresponds with size
                let itemSize = interTC[i].size;
                let itemNoEmpl = interTC[i].employees;
                console.log(itemSize);
                switch (itemSize) {
                    case 'Small':
                        expect(itemNoEmpl).to.be.at.most(10);
                        break;
                    case 'Medium':
                        expect(itemNoEmpl).to.be.at.least(11)
                        .and.to.be.at.most(1000);
                        break;
                    case 'Big':
                        expect(itemNoEmpl).not.to.be.below(1000);
                        break;
                }
            }
            //}

            //UI for pressing onto client details page and breaking with no contact info 
            //Integration test with small; medium; and large sizes for companies indicated
            
            
            //console.log(JSON.stringify(response));
            //var array0 = JSON.parse(response.text);
            // for (var item in interT.new.customers) { console.log(item);}
            //for (var ent in array1) {var array2 = (array1.contactInfo); expect(array2).toBeNull()}
            //for (var ent in array1) {console.log(ent.name);}
            //expect(array1.).to.exist;
            //expect(response.text["customers"]["id"]["contactInfo"]).to.exist;
            done();
        })
    })
})